package cat.escolapia.damviod.pmdm.snake;

/**
 * Created by pau.moreno on 07/12/2016.
 */
public class Block {
    public int x, y;

    public Block(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
